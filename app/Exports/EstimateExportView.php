<?php

namespace App\Exports;

use App\Models\CostEstimateNote;
use App\Models\SubTour;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EstimateExportView implements FromView
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view():View
    {
        $subTour = SubTour::findorFail($this->id);
        $estimates = CostEstimateNote::where('subTour_id', 'LIKE', $subTour->id)->get();
        $costEstimateNote = new CostEstimateNote();
        $totalEstimate = $costEstimateNote->totalEstimate($this->id);
        $total = $totalEstimate * 1.5;
        $tax = $total * 0.1;
        $totalHasTax = $total - $tax;
        $profit = $totalHasTax - $totalEstimate;
        return view('admin.estimates.table', [
            'id' => $this->id,
            'estimates' => $estimates,
            'subTour' => $subTour,
            'total' => $total,
            'totalEstimate' => $totalEstimate,
            'tax' => $tax,
            'totalHasTax' => $totalHasTax,
            'profit' => $profit
        ]);
    }
}
