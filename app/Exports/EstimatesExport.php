<?php

namespace App\Exports;

use App\Models\CostEstimateNote;
use Maatwebsite\Excel\Concerns\FromCollection;

class EstimatesExport implements FromCollection
{
    public function collection()
    {
        return CostEstimateNote::all();
    }

    public function headings()
    {
        return [
            'Order', 'Content', 'Quantity', 'Day', 'Price', 'Total'
        ];
    }
}
