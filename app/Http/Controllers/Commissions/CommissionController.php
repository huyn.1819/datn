<?php

namespace App\Http\Controllers\Commissions;

use App\Http\Controllers\Controller;
use App\Services\CommissionService;
use App\Services\ContractService;
use App\Services\UserService;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    protected $commisionService;
    protected $userService;

    public function __construct(CommissionService $commisionService, ContractService $contractService, UserService $userService)
    {
        $this->commisionService = $commisionService;
        $this->contractService = $contractService;
        $this->userService = $userService;
    }

    public function index()
    {
        $contractId = request()->route('contractId');
        $commissions = $this->commisionService->index($contractId);
        return view('admin.commissions.index', compact('commissions', 'contractId'));
    }

    public function create()
    {
        $contractId = preg_replace('/[^0-9]/','',url()->previous());
        $contract = $this->contractService->findById($contractId);
        $contracts = $this->contractService->getAllWithoutPaginate();
        $users = $this->userService->getAllWithoutPaginate();
        $isStaff = [];

        foreach($users as $user) {
            if($user->roles[0]->name !== "customer") {
                array_push($isStaff, $user);
            }
        } 
        return view('admin.commissions.create', compact('contract', 'isStaff'));
    }

    public function store(Request $request)
    {
        $this->commisionService->create($request);
        $contractId = preg_replace('/[^0-9]/','',url()->previous());
        return redirect()->route('commissions.index', ['contractId' => $contractId]);
    }

    public function edit($id)
    {
        $contractId = preg_replace('/[^0-9]/','',url()->previous());
        $contract = $this->contractService->findById($contractId);
        $commission = $this->commisionService->findById($id);
        return view('admin.commissions.edit', compact('commission', 'contract'));
    }

    public function update(Request $request, int $id)
    {
        $this->commisionService->update($request, $id);
        $commission = $this->commisionService->findById($id);
        return redirect()->route('commissions.index', ['contractId' => $commission->contract_id]);
    }

    public function destroy($id)
    {
        $this->commisionService->destroy($id);
        $contractId = preg_replace('/[^0-9]/','',url()->previous());
        return redirect()->route('commissions.index', ['contractId' => $contractId]);
    }
}