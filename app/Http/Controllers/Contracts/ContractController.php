<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\ContractHistory;
use App\Services\ContractHistoryService;
use App\Services\ContractService;
use App\Services\UserService;
use App\Services\SubTourService;
use Illuminate\Http\Request;

class ContractController extends Controller
{

    protected $contractService;
    protected $contractHistoryService;
    protected $userService;
    protected $subTourService;
    protected $contract;

    public function __construct(ContractService $contractService, ContractHistoryService $contractHistoryService, UserService $userService, SubTourService $subTourService, Contract $contract)
    {
        $this->contractService = $contractService;
        $this->contractHistoryService = $contractHistoryService;
        $this->userService = $userService;
        $this->subTourService = $subTourService;
        $this->contract = $contract;
    }

    public function index($id)
    {
        $subTour = $this->subTourService->findById((int)$id);
        $contracts = $this->contractService->index($subTour->id);
        $subTourId = request()->route('subTourId');
        return view('admin.contracts.index', compact('contracts', 'subTourId'));
    }

    public function create()
    {
        $users = $this->userService->getAllWithoutPaginate();
        $isStaff = [];
        $isCustomer = [];
        $subTours = $this->subTourService->getAllServiceWithoutPaginate();
        $subTourId = preg_replace('/[^0-9]/','',url()->previous());
        
        foreach($users as $user) {
            if($user->roles[0]->name !== "customer") {
                array_push($isStaff, $user);
            } else {
                array_push($isCustomer, $user);
            }
        }
        return view('admin.contracts.create', compact('isStaff', 'isCustomer', 'subTourId'));
    }

    public function store(Request $request)
    {
        $this->contractService->create($request);
        $subTourId = preg_replace('/[^0-9]/','',url()->previous());
        return redirect()->route('contracts.index', ['subTourId' => $subTourId]);
    }

    public function show(int $id)
    {
        $contract = $this->contractService->show($id);
        return view('admin.contracts.show', compact('contract'));
    }

    public function edit($id)
    {
        $contract = $this->contractService->findById($id);
        $users = $this->userService->getAllWithoutPaginate();
        $isStaff = [];
        $isCustomer = [];
        
        foreach($users as $user) {
            if($user->roles[0]->name !== "customer") {
                array_push($isStaff, $user);
            } else {
                array_push($isCustomer, $user);
            }
        }
        return view('admin.contracts.edit', compact('contract', 'isStaff', 'isCustomer'));
    }

    public function update(Request $request, int $id)
    {
        $contract = $this->contractService->update($request, $id);
        $dataCreateContractHistory = [
            'create_by' => $contract->user->full_name,
            'reason_edit' => $request->reason_edit,
            'contract_id' => $id
        ];
        $contractHistory = $this->contractHistoryService->create($dataCreateContractHistory);
        $contractHistory->contract($id);
        $subTourId = $request->sub_tour_id;
        return redirect()->route('contracts.index', ['subTourId' => $subTourId]);
    }

    public function destroy($id)
    {
        $this->contractService->destroy($id);
        $subTourId = preg_replace('/[^0-9]/','',url()->previous());
        return redirect()->route('contracts.index', ['subTourId' => $subTourId]);
    }

    public function createContractFile($id)
    {
        return $this->contractService->createContractFile($id);
    }

    public function search(Request $request)
    {
        $key = $request->input('key') ?? '';
        $result = $this->contract->search($key);
        return view('admin.contracts.index', ['roles' => $result]);
    }

    public function history($id)
    {
        $historyContracts = ContractHistory::where('contract_id', $id)->get();
        return view('admin.contracts.history', [
            'contractId' => $id,
            'historyContracts' => $historyContracts,
        ]);
    }
}
