<?php

namespace App\Http\Controllers\Estimates;

use App\Exports\EstimateExportView;
use App\Http\Controllers\Controller;
use App\Services\EstimateNoteService;
use App\Services\SubTourService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EstimateController extends Controller
{
    protected $estimateNoteService;
    protected $subTourService;

    public function __construct(EstimateNoteService $estimateNoteService, SubTourService $subTourService)
    {
        $this->estimateNoteService = $estimateNoteService;
        $this->subTourService = $subTourService;
    }

    public function index($id)
    {
        $subTour = $this->subTourService->findById((int)$id);
        $estimates = $this->estimateNoteService->index($subTour->id);
        $totalEstimate = $this->estimateNoteService->totalEstimate((int)$id);
        $total = $totalEstimate * 1.5;
        $tax = $total * 0.1;
        $totalHasTax = $total - $tax;
        $profit = $totalHasTax - $totalEstimate;
        return view('admin.estimates.index', compact('estimates', 'subTour', 'totalEstimate', 'total', 'totalHasTax', 'profit', 'tax', 'id'));
    }

    public function store(Request $request)
    {
        $estimate = $this->estimateNoteService->create($request);
        return redirect()->route('estimates.index', $estimate->subTour_id);
    }

    public function update(Request $request, $id)
    {
        $estimate = $this->estimateNoteService->update($request, $id);
        return redirect()->route('estimates.index', $estimate->subTour_id);
    }

    public function destroy($id)
    {
        $estimate = $this->estimateNoteService->destroy($id);
        return redirect()->route('estimates.index', $estimate->subTour_id);
    }

    public function export($id)
    {
        $subTour = $this->subTourService->findById((int)$id);
        $tourCode = $subTour->tour->tour_code;
        return Excel::download(new EstimateExportView($id), date('Ymd') . '_'. $tourCode . '_estimate.xlsx');
    }
}