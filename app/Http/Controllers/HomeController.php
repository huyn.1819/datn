<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Contract;
use App\Services\ContractService;
use App\Services\UserService;
use App\Services\EstimateNoteService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected $contractService;
    protected $userService;
    protected $estimateNoteService;

    public function __construct(ContractService $contractService, UserService $userService, EstimateNoteService $estimateNoteService)
    {
        $this->middleware('auth');
        $this->contractService = $contractService;
        $this->userService = $userService;
        $this->estimateNoteService = $estimateNoteService;
    }

    public function index(Request $request)
    {
        $contracts = $this->contractService->getAllWithoutPaginate();
        $users = $this->userService->getAllWithoutPaginate();
        $isStaff = [];
        $yearSelectedContract = Carbon::now()->year;
        $yearSelectedProfit = Carbon::now()->year;
        $yearSelectedStaff = Carbon::now()->year;
        $monthSelectedStaff = Carbon::now()->month;
        $staffName = null;
        $contractsOfStaffByMonthandYear = [];
        $contractsOfStaffByMonthandYearandNameStaff = [];
        $contractsOfStaffByYearandNameStaff = [];

        foreach($users as $user) {
            if($user->roles[0]->name !== "customer") {
                array_push($isStaff, $user);
            }
        } 

        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $numberContractPerMonth = [];

        if ($request->has('year_contract') && $request->get('year_contract')) {
            $yearSelectedContract = $request->get('year_contract');
        }
        if ($request->has('year_profit') && $request->get('year_profit')) {
            $yearSelectedProfit = $request->get('year_profit');
        }
        foreach ($months as $month) {
            $contract = DB::table('contracts')
                            ->whereYear('created_at', $yearSelectedContract)
                            ->whereMonth('created_at', $month)
                            ->get();
            array_push($numberContractPerMonth, count($contract));
        }

        if ($request->has('year_staff') && $request->get('year_staff')) {
            $yearSelectedStaff = $request->get('year_staff');
        }
        if ($request->has('month_staff') && $request->get('month_staff')) {
            $monthSelectedStaff = $request->get('month_staff');
        }
        if ($request->has('staff_name') && $request->get('staff_name')) {
            $staffName = $request->get('staff_name');
        }

        if ($request->has('year_staff') && $request->get('year_staff') !== "--" && $request->has('month_staff') && $request->get('month_staff') !== "--") {
            foreach($isStaff as $staff) {
                $contractOfStaff = Contract::whereYear('created_at', $yearSelectedStaff)
                                        ->whereMonth('created_at', $monthSelectedStaff)
                                        ->where('create_by', $staff->id)
                                        ->get();
                $commissions = Commission::where('user_id', $staff->id)
                    ->whereYear('created_at', $yearSelectedStaff)
                    ->whereMonth('created_at', $monthSelectedStaff)
                    ->get();
                    $commissionOfStaff = 0;
                
                foreach($commissions as $commission) {
                    if ($commission->user->id == $staff->id) {
                       $commissionOfStaff += $commission->contract->subTour->price * $commission->percent * 0.01;
                    }
                }
                $contractsOfStaffByMonthandYear[] = [$staff->full_name, count($contractOfStaff), number_format($commissionOfStaff)];
            }
        } 

        $estimates = $this->estimateNoteService->all();

        $profits = $this->estimateNoteService->totalTour($estimates);

        $profitPerMonth = $this->estimateNoteService->totalPerMonth($yearSelectedProfit);

        return view('admin.dashboard', compact(
            'contracts', 
            'isStaff', 
            'numberContractPerMonth', 
            'profits', 
            'profitPerMonth', 
            'contractsOfStaffByMonthandYear',
        ));
    }
}
