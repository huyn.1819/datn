<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    protected $roleService;
    protected $permissionService;
    protected $role;

    public function __construct(RoleService $roleService, PermissionService $permissionService, Role $role)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->roleService->index();
        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        $permissions = $this->permissionService->getAllPermission();
        $permissionGroups = $this->permissionService->getGroupsPermission();
        return view('admin.roles.create', compact('permissions', 'permissionGroups'));
    }

    public function store(Request $request)
    {
        $this->roleService->store($request);
        return redirect()->route('roles.index');
    }

    public function show($id)
    {
        $role = $this->roleService->show($id);
        return view('admin.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->findById($id);
        $permissions = $this->permissionService->getAllPermission();
        $permissionIds = $this->roleService->getRolePermissionId($id);
        $permissionGroups = $this->permissionService->getGroupsPermission();
        return view('admin.roles.edit', compact('role', 'permissions', 'permissionIds', 'permissionGroups'));
    }

    public function update(Request $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        $this->roleService->destroy($id);
        return redirect()->route('roles.index');
    }

    public function search(Request $request)
    {
        $key = $request->input('key') ?? '';
        $result = $this->role->search($key);
        return view('admin.roles.index', ['roles' => $result]);
    }
}
