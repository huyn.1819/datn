<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Services\ServiceService;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    protected $serviceService;
    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    public function index()
    {
        $services = $this->serviceService->index();
        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function store(Request $request)
    {
        $this->serviceService->create($request);
        return redirect()->route('services.index');
    }

    public function show(int $id)
    {
        $service = $this->serviceService->show($id);
        return view('admin.services.show', compact('service'));
    }

    public function edit($id)
    {
        $service = $this->serviceService->findById($id);
        return view('admin.services.edit', compact('service'));
    }

    public function update(Request $request, int $id)
    {
        $this->serviceService->update($request, $id);
        return redirect()->route('services.index');
    }

    public function destroy($id)
    {
        $this->serviceService->destroy($id);
        return redirect()->route('services.index');
    }
}
