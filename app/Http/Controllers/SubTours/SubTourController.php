<?php

namespace App\Http\Controllers\SubTours;

use App\Http\Controllers\Controller;
use App\Models\SubTour;
use App\Services\SubTourService;
use App\Services\TourService;
use Illuminate\Http\Request;

class SubTourController extends Controller
{
    protected $subTourService;
    protected $tourService;
    protected $subTour;

    public function __construct(SubTourService $subTourService, TourService $tourService, SubTour $subTour)
    {
        $this->subTourService = $subTourService;
        $this->tourService = $tourService;
        $this->subTour = $subTour;
    }

    public function index()
    {
        $subTours = $this->subTourService->index();
        return view('admin.subTours.index', compact('subTours'));
    }

    public function create()
    {
        $tours = $this->tourService->getAllTours();
        return view('admin.subTours.create', compact('tours'));
    }

    public function store(Request $request)
    {
        $this->subTourService->create($request);
        return redirect()->route('subTours.index');
    }

    public function show($id)
    {
        $subTour = $this->subTourService->show($id);
        return view('admin.subTours.show', compact('subTour'));
    }

    public function edit($id)
    {
        $subTour = $this->subTourService->findById($id);
        $tours = $this->tourService->getAllTours();
        return view('admin.subTours.edit', compact('subTour', 'tours'));
    }

    public function update(Request $request, $id)
    {
        $this->subTourService->update($request, $id);
        return redirect()->route('subTours.index');
    }

    public function destroy($id)
    {
        $this->subTourService->destroy($id);
        return redirect()->route('subTours.index');
    }

    public function search(Request $request)
    {
        $key = $request->input('key') ?? '';
        $result = $this->subTour->search($key);
        dd($result);
        return view('admin.tours.index', ['subTours' => $result]);
    }
}
