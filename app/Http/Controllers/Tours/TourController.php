<?php

namespace App\Http\Controllers\Tours;

use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Services\ServiceService;
use App\Services\TourService;
use Illuminate\Http\Request;

class TourController extends Controller
{
    protected $tourService;
    protected $serviceService;
    protected $tour;

    public function __construct(TourService $tourService, ServiceService $serviceService, Tour $tour)
    {
        $this->tourService = $tourService;
        $this->serviceService = $serviceService;
        $this->tour = $tour;
    }

    public function index()
    {
        $tours = $this->tourService->index();
        return view('admin.tours.index', compact('tours'));
    }

    public function create()
    {
        $services = $this->serviceService->getAllService();
        return view('admin.tours.create', compact('services'));
    }

    public function store(Request $request)
    {
        $this->tourService->create($request);
        return redirect()->route('tours.index');
    }

    public function show(int $id)
    {
        $tour = $this->tourService->show($id);
        return view('admin.tours.show', compact('tour'));
    }

    public function edit($id)
    {
        $tour = $this->tourService->findById($id);
        $services = $this->serviceService->getAllService();
        $serviceIds = $this->tourService->getTourServiceId($id);
        return view('admin.tours.edit', compact('tour', 'services', 'serviceIds'));
    }

    public function update(Request $request, int $id)
    {
        $this->tourService->update($request, $id);
        return redirect()->route('tours.index');
    }

    public function destroy($id)
    {
        $this->tourService->destroy($id);
        return redirect()->route('tours.index');
    }

    public function search(Request $request)
    {
        $key = $request->input('key') ?? '';
        $result = $this->tour->search($key);
        return view('admin.tours.index', ['tours' => $result]);
    }
}
