<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;
    protected $user;

    public function __construct(UserService $userService, RoleService $roleService, User $user)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->userService->index();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = $this->roleService->getAllRole();
        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->userService->create($request);
        $roles = $this->roleService->getAllRole();
        return redirect()->route('users.index', compact('roles'));
    }

    public function show(int $id)
    {
        $user = $this->userService->show($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->findById($id);
        $roles = $this->roleService->getAllRole();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, int $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect()->route('users.index');
    }

    public function search(Request $request)
    {
        $key = $request->input('key') ?? '';
        $result = $this->user->search($key);
        return view('admin.users.index', ['users' => $result]);
    }
}
