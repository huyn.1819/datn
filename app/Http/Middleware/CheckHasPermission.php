<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckHasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {
        if ($this->isAuthorize($request, $permission)) {
            return $next($request);
        } else {
            abort(403, 'User does not have the right permissions.');
        }
    }

    /**
     * @param Request $request
     * @param $permission
     * @return bool
     */
    public function isAuthorize(Request $request, $permission): bool
    {
        return $request->user()->hasPermission($permission) || $request->user()->isSuperAdmin();
    }
}
