<?php

namespace App\Http\Traits;

trait CheckHasPermissionTrait
{
    public function hasPermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermissionTo($permission)) {
                return true;
            }
        }
        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('director');
    }
}
