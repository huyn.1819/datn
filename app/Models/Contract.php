<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;

    protected $table = 'contracts';

    protected $fillable = [
        'name',
        'description',
        'content',
        'create_by',
        'customer_id',
        'contract_code',
        'sub_tour_id'
    ];

    public function subTour()
    {
        return $this->belongsTo('App\Models\SubTour');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function contractHistory()
    {
        return $this->hasMany(ContractHistory::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'create_by', 'id');
    }

    public function commission()
    {
        return $this->hasMany(Commission::class);
    }

    public function search($key)
    {
        return Contract::whereHas('user', function ($e) use ($key) {
            $e->where('full_name', 'LIKE', '%' . $key . '%');
        })->get();
    }
}
