<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostEstimateHistory extends Model
{
    use HasFactory;

    protected $table = "cost_estimate_history";

    protected $fillable = [
        'reason_edit'
    ];

    public function subTour()
    {
        return $this->belongsTo(SubTour::class);
    }
}
