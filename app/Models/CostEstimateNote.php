<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CostEstimateNote extends Model
{
    use HasFactory;

    protected $table = "cost_estimate_note";

    protected $fillable = [
        'content',
        'unit_price',
        'type',
        'quantity',
        'day',
        'note',
        'subTour_id'
    ];

    public function subTour()
    {
        return $this->belongsTo('App\Models\SubTour');
    }

    public function totalEstimate($id)
    {
        $estimateSubTours = $this->where('subTour_id', $id)->get();

        $total = 0;

        foreach($estimateSubTours as $item) {
            $total += $item->quantity * $item->day * $item->unit_price;
        }

        return $total;
    }

    public function totalTour($estimates)
    {
        $totalEstimate = 0;

        foreach ($estimates as $item)
        {
            $totalEstimate += $item->quantity * $item->day * $item->unit_price;
        }

        $total = $totalEstimate * 1.5;
        $tax = $total * 0.1;
        $totalHasTax = $total - $tax;
        $profit = $totalHasTax - $totalEstimate;
        
        return $profit;
    }

    public function totalPerMonth($year)
    {
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $profitPerMonth = [];
        
        foreach ($months as $month)
        {
            $estimatePerMonth = $this->whereYear('created_at', $year)
                                    ->whereMonth('created_at', $month)
                                    ->get();
            $profit = $this->totalTour($estimatePerMonth);
            array_push($profitPerMonth, $profit);
        }

        return $profitPerMonth;
    }
}
