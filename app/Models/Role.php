<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends \Spatie\Permission\Models\Role
{
    use HasFactory;

    protected $hidden = ['guard_name', 'pivot'];

    public function scopeSearchByRole($query, $roleName)
    {
        return $roleName ? $query->where('name', 'LIKE', '%' . $roleName . '%') : null;
    }

    public function setGuardNameAttribute($value): string
    {
        return $this->attributes['guard_name'] = 'web';
    }

    public function getRolePermissionId($id)
    {
        return $this->permissions()->where('role_id', $id)->pluck('permission_id');
    }

    public function search($key)
    {
        return Role::where('name', 'LIKE', '%' . $key. '%')->latest('id')->paginate(5);
    }
}
