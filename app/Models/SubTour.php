<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubTour extends Model
{
    use HasFactory;

    protected $table = 'sub_tours';

    protected $fillable = [
        'start_time',
        'end_time',
        'tour_id',
        'price',
        'tour_type',
        'max_passenger',
        'number_of_day',
        'number_of_night',
        'description',
        'schedule',
        'create_by'
    ];

    public function tour()
    {
        return $this->belongsTo('App\Models\Tour');
    }

    public function contract()
    {
        return $this->hasOne('App\Models\Contract');
    }

    public function estimate()
    {
        return $this->hasOne('App\Models\CostEstimateNote');
    }

    public function estimateHistory()
    {
        return $this->hasMany(CostEstimateHistory::class);
    }

    public function search($key)
    {
        return SubTour::whereHas('tour', function($e) use ($key) {
            $e->where('name', 'LIKE', '%' . str_replace(" ","",$key) . '%')->get();
        });
    }
}
