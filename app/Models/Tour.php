<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use HasFactory;

    protected $table = 'tours';

    protected $fillable = [
        'name',
        'content',
        'start_location',
        'destination_location',
        'create_by',
        'tour_code',
        'tour_type'
    ];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name. '%') : null;
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'tour_has_services', 'tour_id', 'service_id');
    }

    public function attachService($serviceId)
    {
        return $this->services()->attach($serviceId);
    }

    public function syncService($serviceId)
    {
        return $this->services()->sync($serviceId);
    }

    public function detachService()
    {
        $this->services()->detach();
    }

    public function getTourServiceId($id)
    {
        return $this->services()->where('tour_id', $id)->pluck('service_id');
    }

    public function subTour()
    {
        return $this->hasMany('App\Models\SubTour');
    }

    public function search($key)
    {
        return Tour::where('tour_code', 'LIKE', '%' . $key. '%')->latest('id')->paginate(5);
    }
}
