<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    /**
     * @param $model
     */
    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function updateOrCreate(array $dataCheck, array $dataUpdate = [])
    {
        return $this->model->updateOrCreate($dataCheck, $dataUpdate);
    }

    abstract protected function model();

}
