<?php

namespace App\Repositories;

use App\Models\Commission;

class CommissionRepository extends BaseRepository
{
    protected function model()
    {
        return Commission::class;
    }

    public function index($id)
    {
        return $this->model->where('contract_id', 'LIKE', $id)->latest('id')->paginate(10);
    }

    public function getAllService()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }
}
