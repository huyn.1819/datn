<?php

namespace App\Repositories;

use App\Models\ContractHistory;

class ContractHistoryRepository extends BaseRepository
{
    protected function model()
    {
        return ContractHistory::class;
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function getAll()
    {
        return $this->model->latest('id')->paginate(10);
    }
}
