<?php

namespace App\Repositories;

use App\Models\Contract;

class ContractRepository extends BaseRepository
{
    protected function model()
    {
        return Contract::class;
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function getAll($id)
    {
        return $this->model->where('sub_tour_id', 'LIKE', $id)->latest('id')->paginate(10);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function getAllWithoutPaginate()
    {
        return $this->model->all();
    }
}
