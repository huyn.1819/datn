<?php

namespace App\Repositories;

use App\Models\CostEstimateNote;

class EstimateNoteRepository extends BaseRepository
{
    protected function model()
    {
        return CostEstimateNote::class;
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function getAll($id)
    {
        return $this->model->where('subTour_id', 'LIKE', $id)->latest('id')->paginate(10);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function totalEstimate($id)
    {
        return $this->model->totalEstimate($id);
    }

    public function totalTour($estimates)
    {
        return $this->model->totalTour($estimates);
    }

    public function totalPerMonth($year)
    {
        return $this->model->totalPerMonth($year);
    }
}
