<?php

namespace App\Repositories;

use App\Models\Permission;

class PermissionRepository extends BaseRepository
{
    public function model()
    {
        return Permission::class;
    }

    public function getAllPermission()
    {
        return $this->model->all();
    }

    public function getGroupsPermission()
    {
        return $this->model->all()->groupBy('group_name');
    }
}
