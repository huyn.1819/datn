<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    protected function model()
    {
        return Role::class;
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function getAll()
    {
        return $this->model->latest('id')->paginate(10);
    }

    public function getAllRole()
    {
        return $this->model->all();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getRolePermissionId($id)
    {
        return $this->model->find($id)->getRolePermissionId($id);
    }
}
