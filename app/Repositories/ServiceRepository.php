<?php

namespace App\Repositories;

use App\Models\Service;

class ServiceRepository extends BaseRepository
{
    protected function model()
    {
        return Service::class;
    }

    public function getAllServicePaginate()
    {
        return $this->model->latest('id')->paginate(10);
    }

    public function getAllService()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }
}
