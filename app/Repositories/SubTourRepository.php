<?php

namespace App\Repositories;

use App\Models\SubTour;

class SubTourRepository extends BaseRepository
{
    protected function model()
    {
        return SubTour::class;
    }

    public function getAllServicePaginate()
    {
        return $this->model->latest('id')->paginate(10);
    }

    public function getAllServiceWithoutPaginate()
    {
        return $this->model->all();
    }

    public function getAllService()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }
}
