<?php

namespace App\Repositories;

use App\Models\Tour;

class TourRepository extends BaseRepository
{
    protected function model()
    {
        return Tour::class;
    }

    public function getAll()
    {
        return $this->model->latest('id')->paginate(10);
    }

    public function getAllTours()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function getTourServiceId($id)
    {
        return $this->model->find($id)->getTourServiceId($id);
    }
}
