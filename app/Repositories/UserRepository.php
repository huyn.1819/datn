<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    protected function model()
    {
        return User::class;
    }

    public function getAll()
    {
        return $this->model->latest('id')->paginate(10);
    }

    public function getAllWithoutPaginate()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }
}
