<?php

namespace App\Services;

use App\Repositories\CommissionRepository;

class CommissionService
{
    protected $commisionRepository;

    public function __construct(CommissionRepository $commisionRepository)
    {
        $this->commisionRepository = $commisionRepository;
    }

    public function index($id)
    {
        return $this->commisionRepository->index($id);
    }

    public function getAllService()
    {
        return $this->commisionRepository->getAllService();
    }

    public function findById($id)
    {
        return $this->commisionRepository->findById($id);
    }

    public function show($id)
    {
        return $this->commisionRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->commisionRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $tour = $this->commisionRepository->findById($id);
        $dataUpdate = $request->all();
        $tour->update($dataUpdate);
        return $tour;
    }

    public function destroy($id)
    {
        $tour = $this->commisionRepository->findById($id);
        $tour->delete();
        return $tour;
    }
}
