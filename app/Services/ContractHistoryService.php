<?php

namespace App\Services;

use App\Repositories\ContractHistoryRepository;

class ContractHistoryService
{
    protected $contractHistoryRepository;

    public function __construct(ContractHistoryRepository $contractHistoryRepository)
    {
        return $this->contractHistoryRepository = $contractHistoryRepository;
    }

    public function index()
    {
        return $this->contractHistoryRepository->getAll();
    }

    public function create($request)
    {
        return $this->contractHistoryRepository->create($request);
    }

}
