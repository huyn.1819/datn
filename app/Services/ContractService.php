<?php

namespace App\Services;

use App\Repositories\ContractHistoryRepository;
use App\Repositories\ContractRepository;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\TemplateProcessor;

class ContractService
{
    protected $contractRepository;
    protected $contractHistoryRepository;

    public function __construct(ContractRepository $contractRepository, ContractHistoryRepository $contractHistoryRepository)
    {
        $this->contractRepository = $contractRepository;
        $this->contractHistoryRepository = $contractHistoryRepository;
    }

    public function index($id)
    {
        return $this->contractRepository->getAll($id);
    }

    public function getAllWithoutPaginate()
    {
        return $this->contractRepository->getAllWithoutPaginate();
    }

    public function findById($id)
    {
        return $this->contractRepository->findById($id);
    }

    public function show($id)
    {
        return $this->contractRepository->findById($id);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->all();
        $contract = $this->contractRepository->create($dataCreate);
        $contract->customer($request->customer_id);
        $contract->subTour($request->sub_tour_id);
        return $contract;
    }

    public function update($request, $id)
    {
        $contract = $this->contractRepository->findById($id);
        $dataUpdate = $request->all();
        $contract->update($dataUpdate);
        $contract->customer($request->customer_id);
        $contract->subTour($request->sub_tour_id);
        return $contract;
    }

    public function destroy($id)
    {
        $contract = $this->contractRepository->findById($id);
        $contract->delete();
        return $contract;
    }

    /**
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function createContractFile($id)
    {
        $contract = $this->contractRepository->findById($id);
        $contractDocument = new TemplateProcessor(storage_path('Hop_Dong_Du_Lich.docx'));
        $contractDocument->setValues([
            'tour' => $contract->subTour->tour->name,
            'customer_name' => $contract->customer->full_name,
            'address_customer' => $contract->customer->address,
            'phone_number' => $contract->customer->phone,
            'max_passenger' => $contract->subTour->max_passenger,
            'number_of_day' => $contract->subTour->number_of_day,
            'number_of_night' => $contract->subTour->number_of_night,
            'start_time' => \Carbon\Carbon::parse($contract->subTour->start_time)->format('d-m-Y'),
            'end_time' => \Carbon\Carbon::parse($contract->subTour->end_time)->format('d-m-Y'),
            'price' => $contract->subTour->price,
        ]);
        try {
            $contractDocument->saveAs(storage_path('Contract_' . $contract->customer->full_name . date('Ymd') . '.docx'));
        } catch (Exception $e) {

        }
        return response()->download(storage_path('Contract_' . $contract->customer->full_name . date('Ymd') . '.docx'));
    }
}
