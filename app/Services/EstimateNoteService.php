<?php

namespace App\Services;

use App\Repositories\EstimateNoteRepository;

class EstimateNoteService
{
    protected $estimateNoteRepository;

    public function __construct(EstimateNoteRepository $estimateNoteRepository)
    {
        $this->estimateNoteRepository = $estimateNoteRepository;
    }

    public function index($id)
    {
        return $this->estimateNoteRepository->getAll($id);
    }

    public function all()
    {
        return $this->estimateNoteRepository->all();
    }

    public function findById($id)
    {
        return $this->estimateNoteRepository->findById($id);
    }

    public function show($id)
    {
        return $this->estimateNoteRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->estimateNoteRepository->store($dataCreate);
    }

    public function update($request, $id)
    {
        $estimate = $this->estimateNoteRepository->findById($id);
        $dataUpdate = $request->all();
        $estimate->update($dataUpdate);
        return $estimate;
    }

    public function destroy($id)
    {
        $estimate = $this->estimateNoteRepository->findById($id);
        $estimate->delete();
        return $estimate;
    }

    public function totalEstimate($id)
    {
        return $this->estimateNoteRepository->totalEstimate($id);
    }

    public function totalTour($estimates)
    {
        return $this->estimateNoteRepository->totalTour($estimates);
    }

    public function totalPerMonth($year)
    {
        return $this->estimateNoteRepository->totalPerMonth($year);
    }
}
