<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAllPermission()
    {
        return $this->permissionRepository->getAllPermission();
    }

    public function getGroupsPermission()
    {
        return $this->permissionRepository->getGroupsPermission();
    }
}
