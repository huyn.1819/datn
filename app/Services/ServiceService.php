<?php

namespace App\Services;

use App\Repositories\ServiceRepository;

class ServiceService
{
    protected $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function index()
    {
        return $this->serviceRepository->getAllServicePaginate();
    }

    public function getAllService()
    {
        return $this->serviceRepository->getAllService();
    }

    public function findById($id)
    {
        return $this->serviceRepository->findById($id);
    }

    public function show($id)
    {
        return $this->serviceRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->serviceRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $tour = $this->serviceRepository->findById($id);
        $dataUpdate = $request->all();
        $tour->update($dataUpdate);
        return $tour;
    }

    public function destroy($id)
    {
        $tour = $this->serviceRepository->findById($id);
        $tour->delete();
        return $tour;
    }
}
