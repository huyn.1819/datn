<?php

namespace App\Services;

use App\Repositories\SubTourRepository;

class SubTourService
{
    protected $subTourRepository;

    public function __construct(SubTourRepository $subTourRepository)
    {
        $this->subTourRepository = $subTourRepository;
    }

    public function index()
    {
        return $this->subTourRepository->getAllServicePaginate();
    }

    public function getAllServiceWithoutPaginate()
    {
        return $this->subTourRepository->getAllServiceWithoutPaginate();
    }

    public function findById($id)
    {
        return $this->subTourRepository->findById($id);
    }

    public function show($id)
    {
        return $this->subTourRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $subTour = $this->subTourRepository->create($dataCreate);
        $subTour->tour($request->tour_id);
        return $subTour;
    }

    public function update($request, $id)
    {
        $subTour = $this->subTourRepository->findById($id);
        $dataUpdate = $request->all();
        $subTour->update($dataUpdate);
        $subTour->tour($request->tour_id);
        return $subTour;
    }

    public function destroy($id)
    {
        $tour = $this->subTourRepository->findById($id);
        $tour->delete();
        return $tour;
    }
}
