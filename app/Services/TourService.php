<?php

namespace App\Services;

use App\Repositories\TourRepository;

class TourService
{
    protected $tourRepository;

    public function __construct(TourRepository $tourRepository)
    {
        $this->tourRepository = $tourRepository;
    }

    public function index()
    {
        return $this->tourRepository->getAll();
    }

    public function getAllTours()
    {
        return $this->tourRepository->getAllTours();
    }

    public function show($id)
    {
        return $this->tourRepository->findById($id);
    }

    public function findById($id)
    {
        return $this->tourRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $tour = $this->tourRepository->create($dataCreate);
        $tour->syncService($request->service);
        return $tour;
    }

    public function update($request, $id)
    {
        $tour = $this->tourRepository->findById($id);
        $dataUpdate = $request->all();
        $tour->update($dataUpdate);
        $tour->syncService($request->service);
        return $tour;
    }

    public function destroy($id)
    {
        $tour = $this->tourRepository->findById($id);
        $tour->delete();
        return $tour;
    }

    public function getTourServiceId($id)
    {
        return $this->tourRepository->getTourServiceId($id);
    }
}
