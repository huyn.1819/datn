<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return $this->userRepository->getAll();
    }

    public function getAllWithoutPaginate()
    {
        return $this->userRepository->getAllWithoutPaginate();
    }

    public function findById($id)
    {
        return $this->userRepository->findById($id);
    }

    public function show($id)
    {
        return $this->userRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = $request->password;
        $user = $this->userRepository->create($dataCreate);
        $user->assignRole($dataCreate['role']);
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->findById($id);
        $dataUpdate = $request->all();
        $user->update($dataUpdate);
        if ($request->has('role')) {
            $user->syncRoles($request->role);
        }
        return $user;
    }

    public function destroy($id)
    {
        $user = $this->userRepository->findById($id);
        $user->delete();
        return $user;
    }
}
