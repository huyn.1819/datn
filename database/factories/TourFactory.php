<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'content' => $this->faker->text(),
            'start_location_id' => $this->faker->numberBetween(1, 100),
            'destination_location_id' => $this->faker->numberBetween(1, 100),
            'create_by' => $this->faker->name(),
            'tour_code' => $this->faker->numberBetween(1, 100),
        ];
    }
}
