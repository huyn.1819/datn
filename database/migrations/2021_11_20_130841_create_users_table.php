<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->char('user_code');
            $table->char('full_name');
            $table->char('email')->unique();
            $table->unsignedBigInteger('phone')->nullable();
            $table->char('password');
            $table->unsignedBigInteger('citizens_id')->nullable();
            $table->char('address');
            $table->char('dob')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
