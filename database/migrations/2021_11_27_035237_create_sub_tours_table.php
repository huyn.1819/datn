<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_tours', function (Blueprint $table) {
            $table->id();
            $table->char('start_time');
            $table->char('end_time');
            $table->bigInteger('price')->nullable();
            $table->integer('tour_type')->nullable();
            $table->integer('max_passenger');
            $table->integer('number_of_day');
            $table->integer('number_of_night');
            $table->string('description')->nullable();
            $table->string('schedule')->nullable();
            $table->string('create_by');
            $table->timestamps();
            $table->unsignedBigInteger('tour_id');
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_tours');
    }
}
