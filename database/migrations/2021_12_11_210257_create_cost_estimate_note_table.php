<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostEstimateNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_estimate_note', function (Blueprint $table) {
            $table->id();
            $table->string('content');
            $table->bigInteger('unit_price');
            $table->integer('quantity');
            $table->integer('day');
            $table->string('note')->nullable();
            $table->unsignedBigInteger('subTour_id');
            $table->timestamps();
            $table->foreign('subTour_id')->references('id')->on('sub_tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_estimate_note');
    }
}
