<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostEstimateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_estimate_history', function (Blueprint $table) {
            $table->id();
            $table->mediumText('reason_edit');
            $table->unsignedBigInteger('edit_by');
            $table->unsignedBigInteger('cost_estimate_id');
            $table->timestamps();
            $table->foreign('edit_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('cost_estimate_id')->references('id')->on('cost_estimate_note')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_estimate_history');
    }
}
