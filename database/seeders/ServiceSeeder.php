<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dual = Service::create([
            'name' => 'Nguoi lon',
            'price' => 500000,
        ]);

        $child = Service::create([
            'name' => 'Tre em',
            'price' => 200000
        ]);

        $homestay = Service::create([
            'name' => 'Homestay',
            'price' => 2000000,
            'address' => 'Tam Dao'
        ]);

        $hotel = Service::create([
            'name' => 'Khach san',
            'price' => 10000000,
            'address' => 'Ho Chi Minh'
        ]);
    }
}
