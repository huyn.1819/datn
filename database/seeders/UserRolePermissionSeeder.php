<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class UserRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        Permission::create(['name' => 'role-list', 'group_name' => 'role']);
        Permission::create(['name' => 'role-create', 'group_name' => 'role']);
        Permission::create(['name' => 'role-edit', 'group_name' => 'role']);
        Permission::create(['name' => 'role-delete', 'group_name' => 'role']);
        Permission::create(['name' => 'user-list', 'group_name' => 'user']);
        Permission::create(['name' => 'user-create', 'group_name' => 'user']);
        Permission::create(['name' => 'user-edit', 'group_name' => 'user']);
        Permission::create(['name' => 'user-delete', 'group_name' => 'user']);
        Permission::create(['name' => 'booking-list', 'group_name' => 'booking']);
        Permission::create(['name' => 'booking-create', 'group_name' => 'booking']);
        Permission::create(['name' => 'booking-edit', 'group_name' => 'booking']);
        Permission::create(['name' => 'booking-delete', 'group_name' => 'booking']);
        Permission::create(['name' => 'contract-list', 'group_name' => 'contract']);
        Permission::create(['name' => 'contract-create', 'group_name' => 'contract']);
        Permission::create(['name' => 'contract-edit', 'group_name' => 'contract']);
        Permission::create(['name' => 'contract-delete', 'group_name' => 'contract']);
        Permission::create(['name' => 'tour-list', 'group_name' => 'tour']);
        Permission::create(['name' => 'tour-create', 'group_name' => 'tour']);
        Permission::create(['name' => 'tour-edit', 'group_name' => 'tour']);
        Permission::create(['name' => 'tour-delete', 'group_name' => 'tour']);
        Permission::create(['name' => 'estimate-list', 'group_name' => 'estimate']);
        Permission::create(['name' => 'estimate-create', 'group_name' => 'estimate']);
        Permission::create(['name' => 'estimate-edit', 'group_name' => 'estimate']);
        Permission::create(['name' => 'estimate-delete', 'group_name' => 'estimate']);
        Permission::create(['name' => 'service-list', 'group_name' => 'service']);
        Permission::create(['name' => 'service-create', 'group_name' => 'service']);
        Permission::create(['name' => 'service-edit', 'group_name' => 'service']);
        Permission::create(['name' => 'service-delete', 'group_name' => 'service']);

        Role::create(['name' => 'director']);
        $saleRole = Role::create(['name' => 'sale']);
        $saleRole->givePermissionTo(['user-list', 'user-create', 'user-edit', 'user-delete',
            'booking-list', 'booking-create', 'booking-edit', 'booking-delete',
            'contract-list', 'contract-create', 'contract-edit', 'contract-delete']);
        $accountantRole = Role::create(['name' => 'accountant']);
        $accountantRole->givePermissionTo(['estimate-list']);
        $managerRole = Role::create(['name' => 'manager']);
        $managerRole->givePermissionTo(Permission::all());
        Role::create(['name' => 'customer']);

        $directorAccount = User::create([
            'user_code' => 1,
            'full_name' => 'Director',
            'email' => 'root@gmail.com',
            'password' => '123456789',
            'phone' => '0961991597',
            'citizens_id' => '125827577',
            'address' => 'Ha Noi',
            'dob' => '18-01-1999'
        ]);
        $directorAccount->assignRole('director');
        $managerAccount = User::create([
            'user_code' => 2,
            'full_name' => 'Manager',
            'email' => 'manager@gmail.com',
            'password' => '123456789',
            'phone' => '0961991597',
            'citizens_id' => '125827577',
            'address' => 'Bac Ninh',
            'dob' => '18-01-1999'
        ]);
        $managerAccount->assignRole('manager');
        $accountantAccount = User::create([
            'user_code' => 3,
            'full_name' => 'Accountant',
            'email' => 'accountant@gmail.com',
            'password' => '123456789',
            'phone' => '0961991597',
            'citizens_id' => '125827577',
            'address' => 'Thuan Thanh',
            'dob' => '18-01-1999'
        ]);
        $accountantAccount->assignRole('accountant');
        $saleAccount = User::create([
            'user_code' => 4,
            'full_name' => 'Sale',
            'email' => 'sale@gmail.com',
            'password' => '123456789',
            'phone' => '0961991597',
            'citizens_id' => '125827577',
            'address' => 'Ha Noi',
            'dob' => '18-01-1999'
        ]);
        $saleAccount->assignRole('sale');
        $customerAccount = User::create([
            'user_code' => 5,
            'full_name' => 'Customer',
            'email' => 'customer@gmail.com',
            'password' => '123456789',
            'phone' => '0961991597',
            'citizens_id' => '125827577',
            'address' => 'Bac Ninh',
            'dob' => '18-01-1999'
        ]);
        $customerAccount->assignRole('customer');
    }
}
