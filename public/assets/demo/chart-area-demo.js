// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
let profitPerMonth = $("#profit-Per-Month").val();
arrayProfitPerMonth = JSON.parse("[" + profitPerMonth + "]");

var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
      label: "Total",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [
        arrayProfitPerMonth[0][0],
        arrayProfitPerMonth[0][1],
        arrayProfitPerMonth[0][2],
        arrayProfitPerMonth[0][3],
        arrayProfitPerMonth[0][4],
        arrayProfitPerMonth[0][5],
        arrayProfitPerMonth[0][6],
        arrayProfitPerMonth[0][7],
        arrayProfitPerMonth[0][8],
        arrayProfitPerMonth[0][9],
        arrayProfitPerMonth[0][10],
        arrayProfitPerMonth[0][11],
      ],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 100000000,
          maxTicksLimit: 50
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
