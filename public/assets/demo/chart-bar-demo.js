// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
let numberContractPerMonth = $("#number-Contract-Per-Month").val();
arrayNumberContractPerMonth = JSON.parse("[" + numberContractPerMonth + "]");

var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
      label: "Contract",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: [
        arrayNumberContractPerMonth[0][0],
        arrayNumberContractPerMonth[0][1],
        arrayNumberContractPerMonth[0][2],
        arrayNumberContractPerMonth[0][3],
        arrayNumberContractPerMonth[0][4],
        arrayNumberContractPerMonth[0][5],
        arrayNumberContractPerMonth[0][6],
        arrayNumberContractPerMonth[0][7],
        arrayNumberContractPerMonth[0][8],
        arrayNumberContractPerMonth[0][9],
        arrayNumberContractPerMonth[0][10],
        arrayNumberContractPerMonth[0][11],
      ],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 12
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 30,
          maxTicksLimit: 15
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
