$('#role').click(function () {
    let checkAll = $('input[id="role"]:checked').val()
    $('#role-list').prop('checked', !!checkAll);
    $('#role-create').prop('checked', !!checkAll);
    $('#role-edit').prop('checked', !!checkAll);
    $('#role-delete').prop('checked', !!checkAll);
});

$('#user').click(function () {
    let checkAll = $('input[id="user"]:checked').val()
    $('#user-list').prop('checked', !!checkAll);
    $('#user-create').prop('checked', !!checkAll);
    $('#user-edit').prop('checked', !!checkAll);
    $('#user-delete').prop('checked', !!checkAll);
});

$('#booking').click(function () {
    let checkAll = $('input[id="booking"]:checked').val()
    $('#booking-list').prop('checked', !!checkAll);
    $('#booking-create').prop('checked', !!checkAll);
    $('#booking-edit').prop('checked', !!checkAll);
    $('#booking-delete').prop('checked', !!checkAll);
});

$('#tour').click(function () {
    let checkAll = $('input[id="tour"]:checked').val()
    $('#tour-list').prop('checked', !!checkAll);
    $('#tour-create').prop('checked', !!checkAll);
    $('#tour-edit').prop('checked', !!checkAll);
    $('#tour-delete').prop('checked', !!checkAll);
});

$('#contract').click(function () {
    let checkAll = $('input[id="contract"]:checked').val()
    $('#contract-list').prop('checked', !!checkAll);
    $('#contract-create').prop('checked', !!checkAll);
    $('#contract-edit').prop('checked', !!checkAll);
    $('#contract-delete').prop('checked', !!checkAll);
});

$('#finalSettlement').click(function () {
    let checkAll = $('input[id="finalSettlement"]:checked').val()
    $('#finalSettlement-list').prop('checked', !!checkAll);
    $('#finalSettlement-create').prop('checked', !!checkAll);
    $('#finalSettlement-edit').prop('checked', !!checkAll);
    $('#finalSettlement-delete').prop('checked', !!checkAll);
});

$('#estimate').click(function () {
    let checkAll = $('input[id="estimate"]:checked').val()
    $('#estimate-list').prop('checked', !!checkAll);
    $('#estimate-create').prop('checked', !!checkAll);
    $('#estimate-edit').prop('checked', !!checkAll);
    $('#estimate-delete').prop('checked', !!checkAll);
});

$('#service').click(function () {
    let checkAll = $('input[id="service"]:checked').val()
    $('#service-list').prop('checked', !!checkAll);
    $('#service-create').prop('checked', !!checkAll);
    $('#service-edit').prop('checked', !!checkAll);
    $('#service-delete').prop('checked', !!checkAll);
});



