@extends('admin.layouts.app')

@section('title')
    <title>Update Commission</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Commission</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('commissions.update', $commission->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Staff Name</label>
                                    <input type="text" class="form-control"
                                           placeholder="Staff Name..." value="{{ $contract->user->full_name }}" readonly>
                                    <input name="user_id" type="hidden" class="form-control"
                                            placeholder="Staff Name..." value="{{ $contract->user->id }}" readonly>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Contract Name</label>
                                    <input type="text" class="form-control"
                                           placeholder="Contract Name..." value="{{ $contract->name }}" readonly>
                                    <input name="contract_id" type="hidden" class="form-control"
                                            placeholder="Contract Name..." value="{{ $contract->id }}" readonly>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Percent</label>
                                    <input name="percent" type="text" class="form-control"
                                           placeholder="Percent..." value={{ $commission->percent }}>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
