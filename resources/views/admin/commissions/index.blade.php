@extends('admin.layouts.app')

@section('title')
    <title>Commission Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Management Commissions</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="row mt-5">
                    <form action="#" class="input-group col-7" method="GET" style="margin-left: 18%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: -3%">
                        <a href="{{route('commissions.create', $contractId)}}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 90% !important;">
                <thead>
                <tr align="center">
                    <th scope="col" style="width: 10% !important;">Order</th>
                    <th scope="col" style="width: 20% !important;">Staff Name</th>
                    <th scope="col" style="width: 20% !important;">Contract Name</th>
                    <th scope="col" style="width: 10% !important;">Percent</th>
                    <th scope="col" style="width: 10% !important;">Commission</th>
                    <th scope="col" style="width: 20% !important;">Action</th>
                </tr>
                </thead>
                <tbody align="center">
                    @foreach($commissions as $key => $commission)
                        <tr>
                            <td scope="row">{{$key+1}}</td>
                            <td scope="row">{{$commission->user->full_name}}</td>
                            <td scope="row">{{$commission->contract->name}}</td>
                            <td scope="row">{{$commission->percent}}</td>
                            <td scope="row">{{ number_format($commission->contract->subTour->price * $commission->percent * 0.01) }} VND</td>
                            <td>
                                <a href="{{route('commissions.edit', $commission->id)}}" class="btn btn-success"><i
                                        class="far fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger btn-delete" onclick="deletecommission({{$commission->id}})"><i
                                        class="fas fa-trash-alt"></i></button>
                                <form action="{{route('commissions.destroy', $commission->id)}}" id="form-{{$commission->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
@endsection

@push('scripts')
    <script>
        function deletecommission(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
