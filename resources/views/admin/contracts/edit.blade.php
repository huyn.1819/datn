@extends('admin.layouts.app')

@section('title')
    <title>Update Contract</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Contract</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('contracts.update', $contract->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control"
                                           placeholder="Name..." value="{{$contract->name}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control"
                                           placeholder="Description..." value="{{$contract->description}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Content</label>
                                    <input name="content" type="text" class="form-control"
                                           placeholder="Content..." value="{{$contract->content}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Create By</label>
                                    <select name="create_by" class="form-control" placeholder="Create By...">
                                        <option value="--">--</option>
                                        @foreach($isStaff as $staff)
                                            <option value="{{$staff->id}}">{{$staff->full_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Customer</label>
                                    <select name="customer_id" class="form-control" placeholder="Customer...">
                                        <option value="--">--</option>
                                        @foreach($isCustomer as $customer)
                                            <option value="{{$customer->id}}">{{$customer->full_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Contract Code</label>
                                    <input name="contract_code" type="text" class="form-control"
                                           placeholder="Contract Code..." value="{{$contract->contract_code}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <!-- <label class="form-label">Sub Tour ID</label> -->
                                    <input name="sub_tour_id" type="text" class="form-control"
                                           placeholder="Sub Tour ID..." value="{{$contract->sub_tour_id}}" readonly hidden>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Reason Edit</label>
                                    <input name="reason_edit" type="text" class="form-control"
                                           placeholder="Reason Edit...">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
