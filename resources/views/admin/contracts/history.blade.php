@extends('admin.layouts.app')

@section('title')
    <title>History Edit Contract</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">History Edit Contract</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 60% !important;">
                <thead>
                <tr align="center">
                    <th scope="col" style="width: 10% !important;">Order</th>
                    <th scope="col" style="width: 15% !important;">Updated By</th>
                    <th scope="col" style="width: 10% !important;">Reason Edit</th>
                    <th scope="col" style="width: 10% !important;">Time</th>
                </tr>
                </thead>
                <tbody align="center">
                @foreach($historyContracts as $key => $history)
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td scope="row">{{$history->create_by}}</td>
                        <td scope="row">{{$history->reason_edit}}</td>
                        <td scope="row">{{$history->created_at->format('d-m-Y')}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
@endsection
