@extends('admin.layouts.app')

@section('title')
    <title>Contract Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1></h1>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Contract Management</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="row mt-5">
                    <form action="#" class="input-group col-9" method="GET" style="margin-left: 9%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: -1%">
                        <a href="{{ route('contracts.create', $subTourId) }}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 98% !important;">
                <thead>
                <tr align="center">
                    <th scope="col">Order</th>
                    <th scope="col">Name</th>
                    <th scope="col" style="width: 20% !important">Description</th>
                    <th scope="col">Content</th>
                    <th scope="col" style="width:15% !important;">Create By</th>
                    <th scope="col" style="width:15% !important;">Customer</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody align="center">
                @foreach($contracts as $key => $contract)
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td scope="row">{{$contract->name}}</td>
                        <td scope="row">{{$contract->description}}</td>
                        <td scope="row">{{$contract->content}}</td>
                        <td scope="row">{{$contract->user->full_name}}</td>
                        <td scope="row">{{$contract->customer->full_name}}</td>
                        <td>
                            <a href="{{route('contracts.show', $contract->id)}}" class="btn btn-primary" title="Show Contract Detail"><i
                                    class="far fa-eye"></i></a> &nbsp;
                            <a href="{{route('contracts.edit', $contract->id)}}" class="btn btn-success"><i
                                    class="far fa-edit"></i></a> &nbsp;
                            <a href="{{route('commissions.index', $contract->id)}}" class="btn btn-warning" title="Commissions"><i
                            class="fa fa-credit-card"></i></a> &nbsp;
                            <button type="submit" class="btn btn-danger btn-delete" onclick="deleteContract({{$contract->id}})" title="Delete Contract"><i
                                    class="fas fa-trash-alt"></i></button> &nbsp;
                            <div class="form-check-inline">
                                <form action="{{route('contracts.destroy', $contract->id)}}" id="form-{{$contract->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                </form>
                                <form action="{{route('contracts.createContractFile', $contract->id)}}" method="GET">
                                    <button type="submit" class="btn btn-info" style="margin-top: -9px" title="Download Contract"><i
                                            class="fa fa-download"></i></button> &nbsp;
                                </form>
                                <a href="{{route('contracts.history', $contract->id)}}" class="btn btn" title="History Edit" style="height: 38px; width: 42px; background: #F5EEEE; margin-top: -9px"><i
                                    class="fa fa-history"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="container-fluid" style="display: flex; justify-content: end; margin-left: -2%;">
                {{$contracts->links()}}
            </div>
@endsection

@push('scripts')
    <script>
        function deleteContract(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
