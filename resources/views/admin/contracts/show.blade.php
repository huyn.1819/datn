@extends('admin.layouts.app')

@section('title')
    <title>Contract Detail</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1 class="mt-4">Contract Detail</h1>
            <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
            </ol>
            <table class="table table-striped">
                <thead>
                <tr align="center">
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Content</th>
                    <th scope="col">Create By</th>
                    <th scope="col">Customer</th>
                </tr>
                </thead>
                <tbody align="center">
                    <tr>
                        <th scope="row">{{$contract->name}}</th>
                        <th scope="row">{{$contract->description}}</th>
                        <th scope="row">{{$contract->content}}</th>
                        <th scope="row">{{$contract->user->full_name}}</th>
                        <th scope="row">{{$contract->customer->full_name}}</th>
                    </tr>
                </tbody>
            </table>
            <div class="container-fluid">
            </div>

@endsection
