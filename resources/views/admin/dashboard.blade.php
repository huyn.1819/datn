@extends('admin.layouts.app')

@section('title')
    <title>HomePage</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Company Statistic</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Company Statistic</li>
                </ol>
                <div class="row justify-content-center">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4" style="font-size: 20px">
                            <div class="card-body">Number of Employees</div>
                            <div class="card-footer d-flex align-items-center justify-content-center">
                                {{ count($isStaff) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4" style="font-size: 20px">
                            <div class="card-body">Number of Contracts</div>
                            <div class="card-footer d-flex align-items-center justify-content-center">
                                {{ count($contracts) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4" style="font-size: 20px">
                            <div class="card-body">Total revenue</div>
                            <div class="card-footer d-flex align-items-center justify-content-center">
                                {{ number_format($profits) }} VND
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                <div class="row mt-5">
                    @php
                        $nowYear = Carbon\Carbon::now()->year;
                    @endphp
                    <form action={{ route('home') }} class="col-xl-6" method="GET">
                        <label for="years">Choose a year: </label>
                        <select name="year_profit">
                            <option value="--">--</option>
                            <option value="{{ $nowYear - 3 }}" {{ request()->get('year_profit') ==  ($nowYear - 3) ? 'selected' : '' }}>{{ $nowYear - 3 }}</option>
                            <option value="{{ $nowYear - 2 }}" {{ request()->get('year_profit') ==  ($nowYear - 2) ? 'selected' : '' }}>{{ $nowYear - 2 }}</option>
                            <option value="{{ $nowYear - 1 }}" {{ request()->get('year_profit') ==  ($nowYear - 1) ? 'selected' : '' }}>{{ $nowYear - 1 }}</option>
                            <option value="{{ $nowYear }}" {{ request()->get('year_profit') ==  ($nowYear) ? 'selected' : '' }}>{{ $nowYear }}</option>
                        </select>
                        <button class="btn btn-info ml-3 mb-2" type="submit">See</button>
                        @if (request()->get('year_profit'))
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-area me-1"></i>
                                    Profits Per Month
                                </div>
                                <div class="card-body">
                                    <canvas id="myAreaChart" width="100%" height="40"></canvas>
                                    <input type="hidden" value="{{ json_encode($profitPerMonth,TRUE) }}" id="profit-Per-Month">
                                </div>
                            </div>
                        @else
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-area me-1"></i>
                                    Profits Per Month
                                </div>
                                <div class="card-body">
                                    <canvas id="myAreaChart" width="100%" height="40"></canvas>
                                    <input type="hidden" value="0" id="profit-Per-Month">
                                </div>
                            </div>
                        @endif
                    </form>
                    <form action={{ route('home') }} class="col-xl-6" method="GET">
                        <label for="years">Choose a year: </label>
                        <select name="year_contract">
                            <option value="--">--</option>
                            <option value="{{ $nowYear - 3 }}" {{ request()->get('year_contract') ==  ($nowYear - 3) ? 'selected' : '' }}>{{ $nowYear - 3 }}</option>
                            <option value="{{ $nowYear - 2 }}" {{ request()->get('year_contract') ==  ($nowYear - 2) ? 'selected' : '' }}>{{ $nowYear - 2 }}</option>
                            <option value="{{ $nowYear - 1 }}" {{ request()->get('year_contract') ==  ($nowYear - 1) ? 'selected' : '' }}>{{ $nowYear - 1 }}</option>
                            <option value="{{ $nowYear }}" {{ request()->get('year_contract') ==  ($nowYear) ? 'selected' : '' }}>{{ $nowYear }}</option>
                        </select>
                        <button class="btn btn-info ml-3 mb-2" type="submit">See</button>
                        @if (request()->get('year_contract'))
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-bar me-1"></i>
                                    Number of contracts per month 
                                </div>
                                <div class="card-body">
                                    <canvas id="myBarChart" width="100%" height="40"></canvas>
                                    <input type="hidden" value="{{ json_encode($numberContractPerMonth,TRUE) }}" id="number-Contract-Per-Month">
                                </div>
                            </div>
                        @else 
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-bar me-1"></i>
                                    Number of contracts per month 
                                </div>
                                <div class="card-body">
                                    <canvas id="myBarChart" width="100%" height="40"></canvas>
                                    <input type="hidden" value="0" id="number-Contract-Per-Month">
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>

            <form class="d-flex justify-content-center">
                <label for="months">Choose a month: </label>
                <select name="month_staff" class="ml-2 mr-5" style="height: 21px">
                    <option value="--" {{ request()->get('month_staff') ==  '--' ? 'selected' : '' }}>--</option>
                    <option value="1" {{ request()->get('month_staff') ==  1 ? 'selected' : '' }}>Jan</option>
                    <option value="2" {{ request()->get('month_staff') ==  2 ? 'selected' : '' }}>Feb</option>
                    <option value="3" {{ request()->get('month_staff') ==  3 ? 'selected' : '' }}>Mar</option>
                    <option value="4" {{ request()->get('month_staff') ==  4 ? 'selected' : '' }}>Apr</option>
                    <option value="5" {{ request()->get('month_staff') ==  5 ? 'selected' : '' }}>May</option>
                    <option value="6" {{ request()->get('month_staff') ==  6 ? 'selected' : '' }}>Jun</option>
                    <option value="7" {{ request()->get('month_staff') ==  7 ? 'selected' : '' }}>Jul</option>
                    <option value="8" {{ request()->get('month_staff') ==  8 ? 'selected' : '' }}>Aug</option>
                    <option value="9" {{ request()->get('month_staff') ==  9 ? 'selected' : '' }}>Sep</option>
                    <option value="10" {{ request()->get('month_staff') ==  10 ? 'selected' : '' }}>Oct</option>
                    <option value="11" {{ request()->get('month_staff') ==  11 ? 'selected' : '' }}>Nov</option>
                    <option value="12" {{ request()->get('month_staff') ==  12 ? 'selected' : '' }}>Dec</option>
                </select>
                <label for="years">Choose a year: </label>
                <select name="year_staff" class="ml-2 mr-5" style="height: 21px" id="year_staff">
                    <option value="--">--</option>
                    <option value="{{ $nowYear - 3 }}" {{ request()->get('year_staff') ==  ($nowYear - 3) ? 'selected' : '' }}>{{ $nowYear - 3 }}</option>
                    <option value="{{ $nowYear - 2 }}" {{ request()->get('year_staff') ==  ($nowYear - 2) ? 'selected' : '' }}>{{ $nowYear - 2 }}</option>
                    <option value="{{ $nowYear - 1 }}" {{ request()->get('year_staff') ==  ($nowYear - 1) ? 'selected' : '' }}>{{ $nowYear - 1 }}</option>
                    <option value="{{ $nowYear }}" {{ request()->get('year_staff') ==  ($nowYear) ? 'selected' : '' }}>{{ $nowYear }}</option>
                </select>
                <button class="btn btn-info ml-4" type="submit" style="height: 23px; align-items: center; text-align: center; display: flex;" id="btn_see">See</button>
            </form>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-2" style="width: 60% !important;">
                    @if (request()->get('year_staff') !== "--" && request()->get('month_staff') !== "--")
                        <thead align="center">
                            <tr align="center">
                                <th scope="col">Order</th>
                                <th scope="col">Full name</th>
                                <th scope="col">Number of Contract</th>
                                <th scope="col">Commission</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            @forelse ($contractsOfStaffByMonthandYear as $key => $contract) 
                                <tr>
                                    <td scope="col">{{ $key + 1}}</td>
                                    <td scope="col">{{ $contract[0] }}</td>
                                    <td scope="col">{{ $contract[1] }}</td>
                                    <td scope="col">{{ $contract[2] }} VND</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No data</td>
                                </tr>
                            @endforelse
                        </tbody>
                    @endif
                </table>
            </div>
        </main>
    </div>
@endsection


