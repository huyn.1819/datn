@extends('admin.layouts.app')

@section('title')
    <title>Estimate</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="d-flex">
                <div class="mt-3 ml-5 col-8">
                    <div style="font-size: 30px">TUYET SINH TRAVEL</div>
                    <div style="margin-left: -5%; margin-top: 2%;">
                    <ol>
                        <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                    </ol>
                </div>
                </div>
                
                <div class="mt-5 justify-content-end" style="margin-left: 10%;">
                    <a href="{{ route('estimates.export', $subTour->id) }}" type="button"
                       class="btn btn-info" onclick="exportExcel()" id="btn-export">
                        Export to Excel
                    </a>
                </div>
            </div>

{{--            create estimate--}}
            <form action="{{ route('estimates.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal fade" id="create-estimate" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel">Create New Estimate</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="col-form-label">Content</label>
                                        <input type="text" class="form-control" name="content" placeholder="Content...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Quantity</label>
                                        <input type="number" class="form-control" name="quantity" placeholder="Quantity...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Day</label>
                                        <input type="number" class="form-control" name="day" placeholder="Day...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Price</label>
                                        <input type="text" class="form-control" name="unit_price" placeholder="Price...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Note</label>
                                        <input type="text" class="form-control" name="note" placeholder="Note...">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="subTour_id" placeholder="Sub Tour..." value="{{$subTour->id}}">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="id" placeholder="ID..." value="{{(int)($subTour->id)}}">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

{{--            edit estimate--}}
            <form method="POST" enctype="multipart/form-data" id="estimate_id">
                @csrf
                @method('PUT')
                <div class="modal fade" id="edit-estimate" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel">Edit Estimate</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="col-form-label">Content</label>
                                        <input type="text" class="form-control" id="content" name="content" placeholder="Content...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Quantity</label>
                                        <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Day</label>
                                        <input type="number" class="form-control" id="day" name="day" placeholder="Day...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Price</label>
                                        <input type="text" class="form-control" id="unit_price" name="unit_price" placeholder="Price...">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Note</label>
                                        <input type="text" class="form-control" id="note" name="note" placeholder="Note...">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="subTour_id" placeholder="Sub Tour..." value="{{$subTour->id}}">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @include('admin.estimates.table')
        </main>
    </div>
@endsection

<script>
    $('#create-estimate').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let recipient = button.data('whatever')
        let modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
    })
    $('#edit-estimate').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let recipient = button.data('whatever')
        let modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
    })

    function editEstimate(id) {
        let estimate = document.getElementById("btn-edit-"+id).value;
        let estimateArray = JSON.parse(estimate);
        $('#estimate_id').attr('action','/estimates/' + estimateArray.id);
        $('#content').val(estimateArray.content)
        $('#quantity').val(estimateArray.quantity)
        $('#day').val(estimateArray.day)
        $('#unit_price').val(estimateArray.unit_price)
        $('#note').val(estimateArray.note)
    }

    function deleteEstimate(id)
    {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                event.preventDefault();
                document.getElementById('form-' + id).submit();
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
    }

    function hideAll(){
        document.getElementById("action").style.display="none";
        document.getElementById("add-estimate").style.display="none";
        document.getElementById("btn-action").style.display="none";
    }

    function exportExcel()
    {
        hideAll();
    }
</script>
