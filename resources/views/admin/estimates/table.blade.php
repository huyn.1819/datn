<div class="col d-flex justify-content-center" >
    <div class="">
        <div style="font-size: 36px; type: bold">
            ESTIMATE TOUR TABLE
        </div>
        <div>
            <span>Tour Code: </span>
            <span>{{ $subTour->tour->tour_code }}</span>
        </div>
        <div>
            <span>Start Day: </span>
            <span>{{ $subTour->start_time }}</span>
            <span style="margin-left: 20%">End Day: </span>
            <span>{{ $subTour->end_time }}</span>
        </div>
        <div>
            <span>Tour: </span>
            <span>{{ $subTour->tour->name }}</span>
        </div>
    </div>
</div>
<div style="display: flex; justify-content: center">
    <table class="table table-bordered mt-4" style="width: 75% !important;">
        <thead>
        <tr align="center">
            <th scope="col" style="width: 0% !important;">Order</th>
            <th scope="col" style="width: 20% !important;">Content</th>
            <th scope="col" style="width: 0% !important;">Quantity</th>
            <th scope="col" style="width: 6% !important;">Day</th>
            <th scope="col" style="width: 15% !important;">Unit Price</th>
            <th scope="col" style="width: 15% !important;">Price(Cost)</th>
            <th scope="col" style="width: 15% !important;">Total</th>
            <th scope="col" style="width: 15% !important;" id="action"></th>
        </tr>
        </thead>
        <tbody align="center">
        @foreach($estimates as $key => $estimate)
            <tr>
                <td scope="row">{{ $key+1 }}</td>
                <td scope="row">{{ $estimate->content }}</td>
                <td scope="row">{{ $estimate->quantity }}</td>
                <td scope="row">{{ $estimate->day }}</td>
                <td scope="row">{{ number_format($estimate->unit_price) }} VND</td>
                <td scope="row">{{ number_format($estimate->quantity * $estimate->day * $estimate->unit_price) }} VND</td>
                <td scope="row">{{ number_format($estimate->quantity * $estimate->day * $estimate->unit_price * 1.5) }} VND</td>
                <td>
                    <button type="button" data-toggle="modal" data-target="#edit-estimate" class="btn btn-success"
                            onclick="editEstimate({{$estimate->id}})" id="btn-edit-{{$estimate->id}}" value="{{ $estimate }}"><i
                            class="far fa-edit"></i></button>
                    <button type="submit" class="btn btn-danger btn-delete"  onclick="deleteEstimate({{$estimate->id}})"><i
                            class="fas fa-trash-alt"></i></button>
                    <form action="{{route('estimates.destroy', $estimate->id)}}" id="form-{{$estimate->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
            <tr>
                <td colspan="5">
                    Total (No tax)
                </td>
                <td>
                    {{ number_format($totalEstimate) }} VND
                </td>
                <td>
                    {{ number_format($total) }} VND
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    Tax (10%)
                </td>
                <td>
                    {{ 0 }} VND
                </td>
                <td>
                    {{ number_format($tax) }} VND
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    Total (Has tax)
                </td>
                <td class="text-danger">
                    {{ number_format($totalEstimate) }} VND
                </td>
                <td class="text-danger">
                    {{ number_format($totalHasTax) }} VND
                </td>
            </tr>
            <tr>
                <td colspan="5">
                Profit
                </td>
                <td colspan="3" class="text-danger">
                    {{ number_format($profit) }} VND
                </td>
            </tr>
            <tr id="add-estimate">
                <td colspan="7">
                    <button type="button" data-toggle="modal" data-target="#create-estimate" style="border: none; background: white">
                        <i class="fas fa-plus"></i>&nbsp;Add New Estimate ...
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
