<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a class="nav-link" href="{{ route('home') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                    Home
                </a>

{{--                user--}}
                <a class="nav-link" href="{{ route('users.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                    Users
                </a>

{{--                role--}}
                <a class="nav-link" href="{{ route('roles.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user-lock"></i></div>
                    Roles
                </a>

{{--                tour--}}
                <a class="nav-link" href="{{ route('tours.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-plane"></i></div>
                    Tours
                </a>

{{--                subtour--}}
                <a class="nav-link" href="{{ route('subTours.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-bus-alt"></i></div>
                    Sub Tours
                </a>

{{--                estimate--}}
{{--                <a class="nav-link" href="{{ route('estimates.index') }}">--}}
{{--                    <div class="sb-nav-link-icon"><i class="fas fa-user-lock"></i></div>--}}
{{--                    Estimates--}}
{{--                </a>--}}

{{--                service--}}
                <a class="nav-link" href="{{ route('services.index') }}">
                    <div class="sb-nav-link-icon"><i class="fab fa-fort-awesome"></i></div>
                    Services
                </a>
            </div>
        </div>
    </nav>
</div>
