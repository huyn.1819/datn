@extends('admin.layouts.app')

@section('title')
    <title>Create Role</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create Role</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{ route('roles.store') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control" placeholder="Name...">
                                </div>
                            </div>

                            <label class="form-label">Permission</label>
                            <div class="mb-3">
                                <input class="form-check-input" type="checkbox" id="checkAll">
                                <label class="form-label">Select All</label>
                                <div class="row ">
                                    @foreach($permissionGroups as $group => $permission)
                                        <div class="col-md-3 mt-3">
                                            <input class="form-check-input select-item" type="checkbox" id="{{ $group }}" />
                                            <label class="form-label" type="checkbox" ><b>{{ $group }}</b></label>
                                            @foreach($permission as $permissionItem)
                                                <div class="card-text">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                               name="permission[]"
                                                               id="{{ $permissionItem->name }}"
                                                               value="{{ $permissionItem->name }}">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            {{ $permissionItem->name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@endsection

@section('js')
    <script defer src="{{ asset('js/create-role.js') }}"></script>
@endsection
