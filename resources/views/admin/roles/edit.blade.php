@extends('admin.layouts.app')

@section('title')
    <title>Update Role</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Role</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('roles.update', $role->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control"
                                           placeholder="Name..." value="{{$role->name}}">
                                </div>
                            </div>
                            <label class="form-label">Permission</label>
                            <div class="mb-3">
                                <input class="form-check-input" type="checkbox" id="checkAll">
                                <label class="form-label">Select All</label>
                                <div class="row ">
                                    @foreach($permissionGroups as $group => $permission)
                                        <div class="col-md-3 mt-3">
                                            <input class="form-check-input select-item" type="checkbox">
                                            <label class="form-label" type="checkbox" ><b>{{ $group }}</b></label>
                                            @foreach($permission as $permissionItem)
                                                <div class="card-text">
                                                    <div class="form-check">
                                                        <input {{$permissionIds->contains($permissionItem->id) ? 'checked' : ''}} class="form-check-input" type="checkbox"
                                                               name="permission[]"
                                                               value="{{ $permissionItem->id }}">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            {{ $permissionItem->name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@endsection
