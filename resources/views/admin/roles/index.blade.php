@extends('admin.layouts.app')

@section('title')
    <title>Role Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Role Management</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ route('home') }}">HomePage</a>
                </ol>
                <div class="row mt-5">
                    <form action="{{route('roles.search')}}" class="input-group col-9" method="GET" style="margin-left: 9%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: -1%">
                        <a href="{{ route('roles.create') }}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 80% !important;">
                <thead>
                    <tr align="center">
                        <th scope="col">Order</th>
                        <th scope="col">Role</th>
                        <th scope="col" style="width: 60% !important;">Permission</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody align="center">
                @foreach($roles as $key => $role)
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td scope="row">{{$role->name}}</td>
                        @if ($role->name === 'director')
                            <td>
                                <b style="font-size: 18px !important;">All permissions.</b>
                            </td>
                        @elseif ($role->name === 'customer')
                            <td>
                                <b style="font-size: 18px !important;">No any permission.</b>
                            </td>
                        @else
                            <td>
                                @foreach($role->permissions as $permission)
                                    <span class="badge badge-light" style="font-size:18px !important;">{{$permission->name}}</span>
                                @endforeach
                            </td>
                        @endif
                        <td style="text-align: center">
                            <a href="{{route('roles.edit', $role->id)}}" class="btn btn-success"><i
                                    class="far fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-delete" onclick="deleteRole({{$role->id}})"><i
                                    class="fas fa-trash-alt"></i></button>
                            <form action="{{route('roles.destroy', $role->id)}}" id="form-{{$role->id}}" method="POST">
                                @method('DELETE')
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="container-fluid" style="display: flex; justify-content: end; margin-left: -9%;">
                {{$roles->links()}}
            </div>
@endsection

@push('scripts')
    <script>
        function deleteRole(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
