@extends('admin.layouts.app')

@section('title')
    <title>Create Service</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create Service</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('services.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control"
                                           placeholder="Name...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Price</label>
                                    <input name="price" type="text" class="form-control"
                                           placeholder="Price...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control"
                                           placeholder="Description...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input name="address" type="text" class="form-control"
                                           placeholder="Address...">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
