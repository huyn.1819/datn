@extends('admin.layouts.app')

@section('title')
    <title>Update Service</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Service</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('services.update', $service->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control"
                                           placeholder="Name..." value="{{$service->name}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Price</label>
                                    <input name="price" type="text" class="form-control"
                                           placeholder="Price..." value="{{$service->price}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control"
                                           placeholder="Description..." value="{{$service->description}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input name="address" type="text" class="form-control"
                                           placeholder="Address..." value="{{$service->address}}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
