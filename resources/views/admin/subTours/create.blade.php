@extends('admin.layouts.app')

@section('title')
    <title>Create Sub Tour</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create Sub Tour</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('subTours.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Start time</label>
                                    <input name="start_time" type="date" class="form-control"
                                           placeholder="Start time...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">End time</label>
                                    <input name="end_time" type="date" class="form-control"
                                           placeholder="End time...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Tour</label>
                                    <select name="tour_id" class="form-control">
                                    <option value="--">--</option>
                                    @foreach($tours as $tour)
                                        <option value="{{$tour->id}}">{{$tour->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Price</label>
                                    <input name="price" type="text" class="form-control"
                                           placeholder="Price...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Max Passenger</label>
                                    <input name="max_passenger" type="number" class="form-control"
                                           placeholder="Max Passenger...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Number of Day</label>
                                    <input name="number_of_day" type="number" class="form-control"
                                           placeholder= "Value...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Number of Night</label>
                                    <input name="number_of_night" type="number" class="form-control"
                                           placeholder="Number of Night...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control"
                                           placeholder="Description...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Schedule</label>
                                    <input name="schedule" type="text" class="form-control"
                                           placeholder="Schedule...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Create By</label>
                                    <input name="create_by" type="text" class="form-control"
                                           placeholder="Create By...">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
