@extends('admin.layouts.app')

@section('title')
    <title>Update Sub Tour</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update Sub Tour</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('subTours.update', $subTour->id)}}" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Start Time</label>
                                    <input name="start_time" type="text" class="form-control"
                                           placeholder="Start Time..." value="{{$subTour->start_time}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">End Time</label>
                                    <input name="end_time" type="text" class="form-control"
                                           placeholder="End Time..." value="{{$subTour->end_time}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Tour</label>
                                    <select name="tour_id" class="form-control">
                                        @foreach($tours as $tour)
                                            <option value="{{$tour->id}}">{{$tour->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Price</label>
                                    <input name="price" type="text" class="form-control"
                                           placeholder="Price..." value="{{$subTour->price}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Max Passenger</label>
                                    <input name="max_passenger" type="number" class="form-control"
                                           placeholder="Max Passenger..." value="{{$subTour->max_passenger}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Number of Day</label>
                                    <input name="number_of_day" type="number" class="form-control"
                                           placeholder= "Value..." value="{{$subTour->number_of_day}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Number of Night</label>
                                    <input name="number_of_night" type="number" class="form-control"
                                           placeholder="Number of Night..." value="{{$subTour->number_of_night}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control"
                                           placeholder="Description..." value="{{$subTour->description}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Schedule</label>
                                    <input name="schedule" type="text" class="form-control"
                                           placeholder="Schedule..." value="{{$subTour->schedule}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Create By</label>
                                    <input name="create_by" type="text" class="form-control"
                                           placeholder="Create By..." value="{{Auth::user()->full_name}}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
