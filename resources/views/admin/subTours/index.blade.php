@extends('admin.layouts.app')

@section('title')
    <title>Sub Tour Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Sub Tour Management</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ route('home') }}">HomePage</a>
                </ol>
                <div class="row mt-5">
                    <form action="#" class="input-group col-9" method="GET" style="margin-left: 9%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: -1%">
                        <a href="{{ route('subTours.create') }}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 80% !important;">
                    <thead>
                    <tr align="center">
                        <th scope="col">Order</th>
                        <th scope="col" style="width: 15% !important;">Tour</th>
                        <th scope="col" style="width: 10% !important;">Start Time</th>
                        <th scope="col" style="width: 10% !important;">End Time</th>
                        <th scope="col" style="width: 10% !important;">Max Passenger</th>
                        <th scope="col" style="width: 10% !important;">Number of Day</th>
                        <th scope="col" style="width: 10% !important;">Number of Night</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    @foreach($subTours as $key=>$subTour)
                        <tr>
                            <td scope="row">{{$key+1}}</td>
                            <td scope="row">{{$subTour->tour->name}}</td>
                            <td scope="row">{{\Carbon\Carbon::parse($subTour->start_time)->format('d-m-Y')}}</td>
                            <td scope="row">{{\Carbon\Carbon::parse($subTour->end_time)->format('d-m-Y')}}</td>
                            <td scope="row">{{$subTour->max_passenger}}</td>
                            <td scope="row">{{$subTour->number_of_day}}</td>
                            <td scope="row">{{$subTour->number_of_night}}</td>
                            <td>
                                <a href="{{route('subTours.show', $subTour->id)}}" class="btn btn-primary" title="Show Sub Tour Detail"><i
                                        class="far fa-eye"></i></a>
                                <a href="{{route('subTours.edit', $subTour->id)}}" class="btn btn-success" title="Edit Sub Tour"><i
                                        class="far fa-edit"></i></a>
                                <a href="{{route('estimates.index', $subTour->id)}}" class="btn btn-warning" title="Estimates"><i
                                        class="fas fa-donate"></i></a>
                                <a href="{{route('contracts.index', $subTour->id)}}" class="btn btn-info" title="Contracts"><i
                                class="fas fa-file-signature"></i></a>
                                <button type="submit" class="btn btn-danger btn-delete" title="Delete Sub Tour" onclick="deleteTour({{$subTour->id}})"><i
                                        class="fas fa-trash-alt"></i></button>
                                <form action="{{route('subTours.destroy', $subTour->id)}}" id="form-{{$subTour->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="container-fluid" style="    display: flex; justify-content: end; margin-left: -9%;">
                {{$subTours->links()}}
            </div>
@endsection

@push('scripts')
    <script>
        function deleteSubTour(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
