@extends('admin.layouts.app')

@section('title')
    <title>Sub Tour Detail</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1></h1>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Sub Tour Detail</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered" style="width: 80% !important;">
                    <thead>
                    <tr align="center">
                        <th scope="col">ID</th>
                        <th scope="col">Tour</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Status</th>
                        <th scope="col">Max Passenger</th>
                        <th scope="col">Number of Day</th>
                        <th scope="col">Number of Night</th>
                        <th scope="col">Description</th>
                        <th scope="col">Schedule</th>
                        <th scope="col">Create By</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td scope="row">{{$subTour->id}}</td>
                            <td scope="row">{{$subTour->tour->name}}</td>
                            <td scope="row">{{$subTour->start_time}}</td>
                            <td scope="row">{{$subTour->end_time}}</td>
                            <td scope="row">{{$subTour->status}}</td>
                            <td scope="row">{{$subTour->max_passenger}}</td>
                            <td scope="row">{{$subTour->number_of_day}}</td>
                            <td scope="row">{{$subTour->number_of_night}}</td>
                            <td scope="row">{{$subTour->description}}</td>
                            <td scope="row">{{$subTour->schedule}}</td>
                            <td scope="row">{{$subTour->create_by}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
@endsection
