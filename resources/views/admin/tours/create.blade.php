@extends('admin.layouts.app')

@section('title')
    <title>Create Tour</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create Tour</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('tours.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control"
                                           placeholder="Tour Name...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Content</label>
                                    <input name="content" type="text" class="form-control"
                                           placeholder="Content...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Start Location</label>
                                    <input name="start_location" type="text" class="form-control"
                                           placeholder="Start Location...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Destination Location</label>
                                    <input name="destination_location" type="text" class="form-control"
                                           placeholder="Destination Location...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Tour Code</label>
                                    <input name="tour_code" type="text" class="form-control"
                                           placeholder="Tour Code...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Create By</label>
                                    <input name="create_by" type="text" class="form-control"
                                           placeholder="Create By...">
                                </div>
                            </div>
                            <label class="form-label">Service</label>
                            <div class="mb-3">
                                <input class="form-check-input" type="checkbox" id="checkAll">
                                <label class="form-label">Select All</label>
                                <div class="row">
                                @foreach($services as $service)
                                    <div class="col-md-3 mt-3">
                                        <input class="form-check-input" type="checkbox"
                                               name="service[]"
                                               value="{{ $service->id }}">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            {{ $service->name }}
                                        </label>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@endsection
