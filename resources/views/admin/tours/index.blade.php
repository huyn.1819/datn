@extends('admin.layouts.app')

@section('title')
    <title>Tour Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1></h1>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Tour Management</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ route('home') }}">HomePage</a>
                </ol>
                <div class="row mt-5">
                    <form action="{{ route('tours.search') }}" class="input-group col-9" method="GET" style="margin-left: 4%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: 4%">
                        <a href="{{ route('tours.create') }}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 90% !important;">
                <thead>
                <tr align="center">
                    <th scope="col">Order</th>
                    <th scope="col">Name</th>
                    <th scope="col">Content</th>
                    <th scope="col">Start Location</th>
                    <th scope="col">Destination Location</th>
                    <th scope="col">Create By</th>
                    <th scope="col">Tour Code</th>
                    <th scope="col">Service</th>
                    <th scope="col" style="width:15% !important">Action</th>
                </tr>
                </thead>
                <tbody align="center">
                @foreach($tours as $key => $tour)
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td scope="row">{{$tour->name}}</td>
                        <td scope="row">{{$tour->content}}</td>
                        <td scope="row">{{$tour->start_location}}</td>
                        <td scope="row">{{$tour->destination_location}}</td>
                        <td scope="row">{{$tour->create_by}}</td>
                        <td scope="row">{{$tour->tour_code}}</td>
                        <td scope="row">
                            @forelse($tour->services as $service)
                                <span class="badge bg-secondary">{{$service->name}}</span>
                            @empty
                                <span><b>This tour doesn't have service</b></span>
                            @endforelse
                        </td>
                        <td>
                            <a href="{{route('tours.show', $tour->id)}}" class="btn btn-primary"><i
                                    class="far fa-eye"></i></a>
                            <a href="{{route('tours.edit', $tour->id)}}" class="btn btn-success"><i
                                    class="far fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-delete" onclick="deleteTour({{$tour->id}})"><i
                                    class="fas fa-trash-alt"></i></button>
                            <form action="{{route('tours.destroy', $tour->id)}}" id="form-{{$tour->id}}" method="POST">
                                @method('DELETE')
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="container-fluid" style="display: flex; justify-content: end; margin-left: -4%;">
                {{$tours->links()}}
            </div>
@endsection
@push('scripts')
    <script>
        function deleteTour(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
