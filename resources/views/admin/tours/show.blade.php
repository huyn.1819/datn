@extends('admin.layouts.app')

@section('title')
    <title>Tour Detail</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1></h1>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Tour Detail</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container-fluid px-4">
                    <input type="search" name="name" value="{{ request('name') }}" class="form-control"
                           placeholder="search ...">
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                <tr align="center">
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Content</th>
                    <th scope="col">Start Location</th>
                    <th scope="col">Destination Location</th>
                    <th scope="col">Create By</th>
                    <th scope="col">Tour Code</th>
                    <th scope="col">Service</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody align="center">
                    <tr>
                        <td scope="row">{{$tour->id}}</td>
                        <td scope="row">{{$tour->name}}</td>
                        <td scope="row">{{$tour->content}}</td>
                        <td scope="row">{{$tour->start_location}}</td>
                        <td scope="row">{{$tour->destination_location}}</td>
                        <td scope="row">{{$tour->create_by}}</td>
                        <td scope="row">{{$tour->tour_code}}</td>
                        <td scope="row">
                            @forelse($tour->services as $service)
                                <span class="badge bg-secondary">{{$service->name}}</span>
                            @empty
                                <span><b>This tour doesn't have service</b></span>
                            @endforelse
                        </td>
                        <td>
                            <form action="{{route('tours.destroy', $tour->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <a href="{{route('tours.show', $tour->id)}}" class="btn btn-primary"><i
                                        class="far fa-eye"></i></a>
                                <a href="{{route('tours.edit', $tour->id)}}" class="btn btn-success"><i
                                        class="far fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger btn-delete" data-action=""><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </main>
    </div>
@endsection
