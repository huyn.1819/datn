@extends('admin.layouts.app')

@section('title')
    <title>Create User</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create User</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">User Code</label>
                                    <input name="user_code" type="text" class="form-control"
                                           placeholder="User Code...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input name="full_name" type="text" class="form-control"
                                           placeholder="Full Name...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <input name="email" type="text" class="form-control"
                                           placeholder="Email...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Phone</label>
                                    <input name="phone" type="text" class="form-control"
                                           placeholder="Phone...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <input name="password" type="password" class="form-control"
                                           placeholder="Password...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Citizens ID</label>
                                    <input name="citizens_id" type="text" class="form-control"
                                           placeholder="Citizens ID...">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input name="address" type="text" class="form-control"
                                           placeholder="Address...">
                                </div>
                            </div>
                                <label class="form-label">Date of Birth</label>
                                <input class="form-control" type="date" name="dob" placeholder="Date of Birthday...">
                            <div class="mb-3">
                                <label class="form-label">Role</label>
                                <div class="form">
                                    @foreach($roles as $role)
                                        <div class="row">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"
                                                       name="role" value="{{ $role->id }}">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    {{ $role->name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script type="text/javascript">
        $('.date').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endsection
