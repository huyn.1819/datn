@extends('admin.layouts.app')

@section('title')
    <title>Update User</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Update User</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{route('users.update', $user->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">User Code</label>
                                    <input name="user_code" type="text" class="form-control"
                                           placeholder="User Code..." value="{{$user->user_code}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input name="full_name" type="text" class="form-control"
                                           placeholder="Full Name..." value="{{$user->full_name}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <input name="email" type="text" class="form-control"
                                           placeholder="Email..." value="{{$user->email}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Phone</label>
                                    <input name="phone" type="text" class="form-control"
                                           placeholder="Phone..." value="{{$user->phone}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Citizens ID</label>
                                    <input name="citizens_id" type="text" class="form-control"
                                           placeholder="Citizens ID..." value="{{$user->citizens_id}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input name="address" type="text" class="form-control"
                                           placeholder="Address..." value="{{$user->address}}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Date of Birthday</label>
                                    <input name="dob" type="date" class="form-control"
                                           placeholder="Date of Birthday..." value="{{$user->dob}}">
                                </div>
                            </div>
                            <div class="form">
                                @foreach($roles as $role)
                                    <div class="row">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox"
                                                   name="role" value="{{ $role->id }}">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                {{ $role->name }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-success" >Update User</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
