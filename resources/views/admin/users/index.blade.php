@extends('admin.layouts.app')

@section('title')
    <title>User Management</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <h1></h1>
            <div class="container-fluid px-4">
                <h1 class="mt-4">User Management</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ route('home') }}">HomePage</a>
                </ol>
                <div class="row mt-5">
                    <form action=" {{ route('users.search') }}" class="input-group col-9" method="GET" style="margin-left: 9%">
                        <input type="search" class="form-control rounded col-5" placeholder="Search" name="key" />
                        <button type="submit" class="btn btn-outline-primary ml-2 col-1">search</button>
                    </form>
                    <div class="col-2" style="margin-left: -1%">
                        <a href="{{ route('users.create') }}" class="btn btn-success col-5">
                            <i class="fas fa-plus-circle"></i>&nbsp;Create
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <table class="table table-bordered mt-5" style="width: 90% !important;">
                <thead>
                <tr align="center">
                    <th scope="col">Order</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Citizens ID</th>
                    <th scope="col">Address</th>
                    <th scope="col">Date of Birth</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody align="center">
                @foreach($users as $key => $user)
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td scope="row">{{$user->full_name}}</td>
                        <td scope="row">{{$user->email}}</td>
                        <td scope="row">0{{$user->phone}}</td>
                        <td scope="row">{{$user->citizens_id}}</td>
                        <td scope="row">{{$user->address}}</td>
                        <td scope="row">{{\Carbon\Carbon::parse($user->dob)->format('d-m-Y')}}</td>
                        @foreach($user->roles as $role)
                            <td scope="row">{{$role->name}}</td>
                        @endforeach
                        <td>
                            <a href="{{route('users.show', $user->id)}}" class="btn btn-primary"><i
                                    class="far fa-eye"></i></a>
                            <a href="{{route('users.edit', $user->id)}}" class="btn btn-success"><i
                                    class="far fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-delete" onclick="deleteUser({{$user->id}})"><i
                                    class="fas fa-trash-alt"></i></button>
                            <form action="{{route('users.destroy', $user->id)}}" id="form-{{$user->id}}" method="POST">
                                @method('DELETE')
                                @csrf

                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="container-fluid d-flex" style="justify-content: flex-end; margin-left: -4%;">
                {{$users->links()}}
            </div>
@endsection
@push('scripts')
    <script>
        function deleteUser(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush
