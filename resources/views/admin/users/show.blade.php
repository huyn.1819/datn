@extends('admin.layouts.app')

@section('title')
    <title>User Detail</title>
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">User Detail</h1>
                <ol class="breadcrumb mb-4">
                    <a class="breadcrumb-item active" href="{{ url()->previous() }}">Back>></a>
                </ol>
            </div>
            <table class="table table-striped">
                <thead>
                <tr align="center">
                    <th scope="col">ID</th>
                    <th scope="col">User Code</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Citizens ID</th>
                    <th scope="col">Address</th>
                    <th scope="col">Date of Birth</th>
                </tr>
                </thead>
                <tbody align="center">
                    <tr>
                        <td scope="row">{{$user->id}}</td>
                        <td scope="row">{{$user->user_code}}</td>
                        <td scope="row">{{$user->full_name}}</td>
                        <td scope="row">{{$user->email}}</td>
                        <td scope="row">{{$user->phone}}</td>
                        <td scope="row">{{$user->citizens_id}}</td>
                        <td scope="row">{{$user->address}}</td>
                        <td scope="row">{{$user->dob}}</td>
                    </tr>
                </tbody>
            </table>
            <div class="container-fluid">
            </div>
@endsection
