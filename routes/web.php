<?php

use App\Http\Controllers\Contracts\ContractController;
use App\Http\Controllers\Estimates\EstimateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Roles\RoleController;
use App\Http\Controllers\Services\ServiceController;
use App\Http\Controllers\SubTours\SubTourController;
use App\Http\Controllers\Tours\TourController;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Commissions\CommissionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])
    ->name('home')
    ->middleware('auth');

Route::group(['middleware' => 'auth'], function() {
    Route::name('tours.')->prefix('tours')->group(function () {
        Route::get('/', [TourController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:tour-list');
        Route::get('/create', [TourController::class, 'create'])
            ->name('create')
            ->middleware('has_permission:tour-create');
        Route::post('/', [TourController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:tour-create');
        Route::get('/edit/{id}', [TourController::class, 'edit'])
            ->name('edit')
            ->middleware('has_permission:tour-edit');
        Route::put('/{id}', [TourController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:tour-edit');
        Route::delete('/{id}', [TourController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:tour-delete');
        Route::get('/show/{id}', [TourController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:tour-list');
        Route::get('/search', [TourController::class, 'search'])
            ->name('search');
    });
    Route::name('users.')->prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:user-list');
        Route::get('/create', [UserController::class, 'create'])
            ->name('create')
            ->middleware('has_permission:user-create');
        Route::post('/', [UserController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:user-create');
        Route::get('/edit/{id}', [UserController::class, 'edit'])
            ->name('edit')
            ->middleware('has_permission:user-edit');
        Route::put('/{id}', [UserController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:user-edit');
        Route::delete('/{id}', [UserController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:user-delete');
        Route::get('/show/{id}', [UserController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:user-list');
        Route::get('/search', [UserController::class, 'search'])
            ->name('search');
    });
    Route::name('services.')->prefix('services')->group(function () {
        Route::get('/', [ServiceController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:service-list');
        Route::get('/create', [ServiceController::class, 'create'])
            ->name('create')
            ->middleware('has_permission:service-create');
        Route::post('/', [ServiceController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:service-create');
        Route::get('/edit/{id}', [ServiceController::class, 'edit'])
            ->name('edit')
            ->middleware('has_permission:service-edit');
        Route::put('/{id}', [ServiceController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:service-edit');
        Route::delete('/{id}', [ServiceController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:service-delete');
        Route::get('/show/{id}', [ServiceController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:service-list');
    });
    Route::name('roles.')->prefix('roles')->group(function () {
        Route::get('/', [RoleController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:role-list');
        Route::get('/create', [RoleController::class, 'create'])
            ->name('create')
            ->middleware('has_permission:role-create');
        Route::post('/', [RoleController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:role-create');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])
            ->name('edit')
            ->middleware('has_permission:role-edit');
        Route::put('/{id}', [RoleController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:role-edit');
        Route::delete('/{id}', [RoleController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:role-delete');
        Route::get('/show/{id}', [RoleController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:role-list');
        Route::get('/search', [RoleController::class, 'search'])
            ->name('search');
    });
    Route::name('subTours.')->prefix('subTours')->group(function () {
        Route::get('/', [SubTourController::class, 'index'])
            ->name('index');
        Route::get('/create', [SubTourController::class, 'create'])
            ->name('create');
        Route::post('/', [SubTourController::class, 'store'])
            ->name('store');
        Route::get('/edit/{id}', [SubTourController::class, 'edit'])
            ->name('edit');
        Route::put('/{id}', [SubTourController::class, 'update'])
            ->name('update');
        Route::delete('/{id}', [SubTourController::class, 'destroy'])
            ->name('destroy');
        Route::get('/show/{id}', [SubTourController::class, 'show'])
            ->name('show');
        Route::get('/search', [SubTourController::class, 'search'])
            ->name('search');
    });

    Route::name('estimates.')->prefix('estimates')->group(function () {
        Route::get('subTour/{subTourId}', [EstimateController::class, 'index'])
            ->name('index');
        Route::get('/create', [EstimateController::class, 'create'])
            ->name('create');
        Route::post('/', [EstimateController::class, 'store'])
            ->name('store');
        Route::get('/edit/{id}', [EstimateController::class, 'edit'])
            ->name('edit');
        Route::put('/{id}', [EstimateController::class, 'update'])
            ->name('update');
        Route::delete('/{id}', [EstimateController::class, 'destroy'])
            ->name('destroy');
        Route::get('/show/{id}', [EstimateController::class, 'show'])
            ->name('show');
        Route::get('/search', [EstimateController::class, 'search'])
            ->name('search');
        Route::get('/export/{id}', [EstimateController::class, "export"])
            ->name("export");
    });

    Route::name('contracts.')->prefix('contracts')->group(function () {
        Route::get('subTour/{subTourId}', [ContractController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:contract-list');
        Route::get('subTour/{subTourId}/create', [ContractController::class, 'create'])
            ->name('create')
            ->middleware('has_permission:contract-create');
        Route::post('/', [ContractController::class, 'store'])
            ->name('store');
        Route::get('/edit/{id}', [ContractController::class, 'edit'])
            ->name('edit')
            ->middleware('has_permission:contract-edit');
        Route::put('/{id}', [ContractController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:contract-edit');
        Route::delete('/{id}', [ContractController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:contract-delete');
        Route::get('/show/{id}', [ContractController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:contract-list');
        Route::get('/search', [ContractController::class, 'search'])
            ->name('search');
        Route::get('/createContract/{id}', [ContractController::class, "createContractFile"])
            ->name("createContractFile");
        Route::get('/contract/{contractId}/history', [ContractController::class, "history"])
            ->name("history");
    });
    Route::name('commissions.')->prefix('commissions')->group(function () {
        Route::get('contract/{contractId}', [CommissionController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:contract-list');
        Route::get('contract/{contractId}/create', [CommissionController::class, 'create'])
            ->name('create');
        Route::post('/', [CommissionController::class, 'store'])
            ->name('store');
        Route::get('/edit/{id}', [CommissionController::class, 'edit'])
            ->name('edit');
        Route::put('/{id}', [CommissionController::class, 'update'])
            ->name('update');
        Route::delete('/{id}', [CommissionController::class, 'destroy'])
            ->name('destroy');
        Route::get('/show/{id}', [CommissionController::class, 'show'])
            ->name('show');
    });

});


